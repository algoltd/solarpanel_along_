<?php

//@ZH | Algoltd | zohar.hadad@algo.team

define("columns_no",3); //change here the number of columns
//echo columns_no;

echo "<!DOCTYPE html><html><head><style>table, th, td {    border: 1px solid black;    border-collapse: collapse;}th, td {    padding: 7px;}th {text-align: center;}\n\n";
echo "</style>";
echo "<body style='background-color: #F5F5F5'>\n";
echo "<table style='width:60%' align='center' text-align='center' >\n";
echo "<tr text-align='center' style='background-color: #FDFDFD' >\n";
echo "<th style='width:10%' text-align='center'><font size='5'>Station</font size='5'> </th>\n";
echo "<th text-align='center'><font size='5'>angle 1 </font size='5'></th>\n";
echo "<th text-align='center'><font size='5'>angle 2 </font size='5'></th>\n";
echo "</tr>\n";


//The two files to read from
$file1_toread="so-csv.csv";
$file2_toread="so-csv2.csv";
//The two column to read from each file (column1 is from file1 and etc')
$column1_toread=1;
$column2_toread=2;

//summing each column's sum and calculating the number of rows
$fp1 = fopen($file1_toread, "r");
$fp2 = fopen($file2_toread, "r");

//echo ($line1 = fgetcsv($fp1));
echo "<tr>";
for ($i=1; $i<=50; $i++){
	echo "<tr>";
	echo "<td><center>" . htmlspecialchars($i) . "</center></td>";
	if ($line1 = fgetcsv($fp1)){
		echo "<td><center>" . htmlspecialchars($line1[$column1_toread]) . "</center></td>";}
	if ($line2 = fgetcsv($fp2)){
		echo "<td><center>" . htmlspecialchars($line2[$column2_toread]) . "</center></td>";}
	echo "</tr>\n";
}
echo "</tr>\n";

fclose($fp1); fclose($fp2);
echo "\n</table></body></html>";
