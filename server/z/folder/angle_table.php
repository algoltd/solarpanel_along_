<?php
// This is a file that gets & displays a .csv file (look below) as a table
// with attributes differs more than the mean of the column + delta (from a file) are highlighted in red
// Take to consideration the number of columns and the files paths!
//@ZH | Algoltd | zohar.hadad@algo.team

define("columns_no",3); //change here the number of columns
//echo columns_no;

echo "<!DOCTYPE html><html><head><style>table, th, td {    border: 1px solid black;    border-collapse: collapse;}th, td {    padding: 7px;}th {text-align: center;}\n\n";
echo "</style>";
echo "<body style='background-color: #F5F5F5'>\n";
echo "<table style='width:60%' align='center' text-align='center' >\n";
echo "<tr text-align='center' style='background-color: #FDFDFD' >\n";
echo "<th style='width:10%' text-align='center'><font size='5'>Station</font size='5'> </th>\n";
echo "<th text-align='center'><font size='5'>angle 1 </font size='5'></th>\n";
echo "<th text-align='center'><font size='5'>angle 2 </font size='5'></th>\n";
echo "</tr>\n";

//reads the delta from a file
$delta=file_get_contents("txt/highlight_deviation.txt");

$rows_no=0;

//summing each column's sum and calculating the number of rows
$fsum = fopen("so-csv.csv", "r");
while (($line = fgetcsv($fsum)) !== false) {
	$rows_no++;
	for ($i=0;  $i<columns_no; $i++){
		$sum[$i]=$sum[$i]+$line[$i];}
}

//summing each column's mean
for ($i=0;  $i<columns_no; $i++){
	$mean[$i]=$sum[$i]/$rows_no;}

//displays the table
$f = fopen("so-csv.csv", "r");
while (($line = fgetcsv($f)) !== false) {
        echo "<tr>";
	for ($i=0;  $i<columns_no; $i++){
		if (htmlspecialchars($line[$i]) > ($mean[$i]+$delta)){
			echo "<td><center><FONT COLOR='red'>" . htmlspecialchars($line[$i]) . "</FONT></center></td>";}
		else
			echo "<td><center>" . htmlspecialchars($line[$i]) . "</center></td>";
	}
        echo "</tr>\n";
}

fclose($f);
echo "\n</table></body></html>";
