<?php
include("class/pData.class.php");
include("class/pDraw.class.php");
include("class/pImage.class.php");

$myData = new pData();
///////////////////////////////////////////////////////////////////////////////
$fp = fopen("data/a.txt", "r") or die("Unable to open file!");
$rec_a_array=file('data/a.txt', FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
$myData->addPoints($rec_a_array,"Serie1");
$myData->setSerieDescription("Serie1","Serie 1");
$myData->setSerieOnAxis("Serie1",0);
fclose($fp);

$fp = fopen("data/b.txt", "r") or die("Unable to open file!");
$rec_b_array=file('data/b.txt', FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
$myData->addPoints($rec_b_array,"Serie2");
$myData->setSerieDescription("Serie2","Serie 2");
$myData->setSerieOnAxis("Serie2",0);

$fp = fopen("data/c.txt", "r") or die("Unable to open file!");
$rec_c_array=file('data/c.txt', FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
$myData->addPoints($rec_c_array,"Serie3");
$myData->setSerieDescription("Serie3","Serie 3");
$myData->setSerieOnAxis("Serie3",0);

$fp = fopen("data/d.txt", "r") or die("Unable to open file!");
$rec_d_array=file('data/d.txt', FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
$myData->addPoints($rec_d_array,"Serie4");
$myData->setSerieDescription("Serie4","Serie 4");
$myData->setSerieOnAxis("Serie4",0);

$fp = fopen("data/e.txt", "r") or die("Unable to open file!");
$rec_e_array=file('data/e.txt', FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
$myData->addPoints($rec_e_array,"Serie5");
$myData->setSerieDescription("Serie5","Serie 5");
$myData->setSerieOnAxis("Serie5",0);

$fp = fopen("data/f.txt", "r") or die("Unable to open file!");
$rec_f_array=file('data/f.txt', FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
$myData->addPoints($rec_f_array,"Serie6");
$myData->setSerieDescription("Serie6","Serie 6");
$myData->setSerieOnAxis("Serie6",0);
///////////////////////////////////////////////////////////////////////////////
//X Axis
$myData->addPoints(array("5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20"),"Absissa");
$myData->setAbscissa("Absissa");

$myData->setAxisPosition(0,AXIS_POSITION_LEFT);
$myData->setAxisName(0,"Angle");
$myData->setAxisUnit(0,"");

//black rect dims
$myPicture = new pImage(1200,400,$myData);
//pic background colors @ZH
$Settings = array("R"=>255, "G"=>255, "B"=>255, "Dash"=>1, "DashR"=>255, "DashG"=>255, "DashB"=>255);

$myPicture->drawFilledRectangle(0,0,899,299,$Settings);

//$Settings = array("StartR"=>219, "StartG"=>231, "StartB"=>139, "EndR"=>1, "EndG"=>138, "EndB"=>68, "Alpha"=>50);
//Gradient @ZH
//$myPicture->drawGradientArea(0,0,0,0,DIRECTION_VERTICAL,$Settings);

//rectangle size (left pos, ver pos, hor size, ver size)
$myPicture->drawRectangle(0,0,1199,399,array("R"=>0,"G"=>0,"B"=>0));

$myPicture->setShadow(TRUE,array("X"=>1,"Y"=>1,"R"=>50,"G"=>50,"B"=>50,"Alpha"=>20));

//header
$myPicture->setFontProperties(array("FontName"=>"fonts/Forgotte.ttf","FontSize"=>18));
$TextSettings = array("Align"=>TEXT_ALIGN_MIDDLEMIDDLE
, "R"=>100, "G"=>100, "B"=>100);
//X from left, Y from top text @ZH 
$myPicture->drawText(100,20,"no text at the moment",$TextSettings);

$myPicture->setShadow(FALSE);
//Graph Area.
//X left start, Top Y start,  X right stop, Button Y stop)
$myPicture->setGraphArea(250,50,1150,350);
$myPicture->setFontProperties(array("R"=>0,"G"=>0,"B"=>0,"FontName"=>"fonts/pf_arma_five.ttf","FontSize"=>8));

$Settings = array("Pos"=>SCALE_POS_LEFTRIGHT
, "Mode"=>SCALE_MODE_FLOATING
, "LabelingMethod"=>LABELING_ALL
, "GridR"=>50, "GridG"=>255, "GridB"=>255, "GridAlpha"=>50, "TickR"=>0, "TickG"=>0, "TickB"=>0, "TickAlpha"=>50, "LabelRotation"=>0, "CycleBackground"=>0, "DrawXLines"=>1, "DrawSubTicks"=>1, "SubTickR"=>255, "SubTickG"=>0, "SubTickB"=>0, "SubTickAlpha"=>50, "DrawYLines"=>ALL);
$myPicture->drawScale($Settings);

$myPicture->setShadow(TRUE,array("X"=>1,"Y"=>1,"R"=>50,"G"=>50,"B"=>50,"Alpha"=>10));

$Config = "";
$myPicture->drawLineChart($Config);

$Config = array("FontR"=>0, "FontG"=>0, "FontB"=>0, "FontName"=>"fonts/pf_arma_five.ttf", "FontSize"=>8, "Margin"=>6, "Alpha"=>30, "BoxSize"=>5, "Style"=>LEGEND_NOBORDER
, "Mode"=>LEGEND_HORIZONTAL
);
//series header location (X from right, Y from top)
$myPicture->drawLegend(550,15,$Config);

$myPicture->stroke();
?>
