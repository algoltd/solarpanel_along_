<?php
   ob_start();
   session_start();
?>

<?
   // error_reporting(E_ALL);
   // ini_set("display_errors", 1);
?>

<html lang = "en">
   
   <head>

      <title>Solar panels - login page</title>
	
      <link href = "css/bootstrap.min.css" rel = "stylesheet">
      
      <style>
         body {
            padding-top: 100px;
            padding-bottom: 100px;
            background-color: #FDFDFD;
         }
         
         .form-signin {
            max-width: 330px;
            padding: 15px;
            margin: 0 auto;
            color: #091D42;
         }
         
         .form-signin .form-signin-heading,
         .form-signin .checkbox {
            margin-bottom: 10px;
         }
         
         .form-signin .checkbox {
            font-weight: normal;
         }
         
         .form-signin .form-control {
            position: relative;
            height: auto;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            padding: 10px;
            font-size: 16px;
         }
         
         .form-signin .form-control:focus {
            z-index: 2;
         }
         
         
         .form-signin input[type="password"] {
            margin-bottom: 10px;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
            border-color:#FFFFFF;
         }
         
         h2{
            text-align: center;
	    font-size:30;
            color: #091D42;
         }
      </style>
      
   </head>
	
   <body>
      

      <div class = "container form-signin">
         
         <?php
//	<h1><center><font size="10"><font color="#1B325F">Solar panels</font color="#1B325F"></font size="10"></center></h1>
	    //$fpsite = fopen("txt/sitename.txt", "r") or die("Unable to open file!");
            $site=file_get_contents("txt/sitename.txt");//reads only the first 4 chars, dont take more because will save the eof
	    echo "<p align='center'> <font color=#1B325F size='10pt'>$site</font></p>";
	    //echo '<span style="color:#AFA;text-align:center;">Request has been sent. Please wait for my reply!</span>';
	    //fclose($fpsite);
            $msg = '';
            $fp = fopen("txt/pass.txt", "r") or die("Unable to open file!");
            //$pass=fgets($fp);
            //$pass=fscanf($fp, "%s");
            $pass=fread($fp,20);//reads only the first 4 chars, dont take more because will save the eof
            fclose($fp);
            if (!empty($_POST['password'])) {
               if ( $_POST['password'] == $pass) {
                  $_SESSION['valid'] = true;
                  $_SESSION['timeout'] = time();
                  $_SESSION['token'] = 1;
                  
                  //echo file_get_contents("folder/redir.html");
		header("Location: ./folder/redir.php"); /* Redirect browser */
		exit();

               }else {
                  $msg = 'Wrong username or password';
               }
            }
         ?>
      </div> <!-- /container -->
      
      <div class = "container">
       <center>
         <form class = "form-signin" role = "form" action = "<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method = "post">
         <h4 class = "form-signin-heading"><?php echo $msg; ?></h4>

         <input type = "password" class = "form-control" name = "password" placeholder = "Enter password" required>
	 <br>
         <button class = "btn btn-block" type = "submit" name = "login">Login</button>
         </form>
	 <br><br><br>	
       </center>
        <center> Click here to clean <a href = "logout.php" tite = "Logout">Session.</center>

         
      </div> 
      
   </body>
</html>
