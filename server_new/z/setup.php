<!DOCTYPE HTML> 
<html>
<head>
</head>
<body style='background-color: #FDFDFD'>

<?php
session_start();
	if( $_SESSION['valid'] == true && $_SESSION['token'] == 1) {
	} else {
	    header("location: login.php");
	}

// define variables and set to empty values
//$sitename = $password = $gender = $site_update_interval = $highlight_deviation = "";
//retrive data from the DB:
$sitename=file_get_contents("txt/sitename.txt");
$password=file_get_contents("txt/pass.txt");
$highlight_deviation=file_get_contents("txt/highlight_deviation.txt");
$site_update_interval=file_get_contents("txt/site_update_interval.txt");
$station_update_interval=file_get_contents("txt/station_update_interval.txt");
$time=file_get_contents("txt/time.txt");
$time_update_interval=file_get_contents("txt/time_update_interval.txt");
$tech1=file_get_contents("txt/tech1.txt");
$tech2=file_get_contents("txt/tech2.txt");
$wind=file_get_contents("txt/wind.txt");
$changed=0;

function test_input($data) { //corrects the input string
//   $data = trim($data);
//   $data = stripslashes($data);
 //  $data = htmlspecialchars($data);
   return $data;
}

if ($_SERVER["REQUEST_METHOD"] == "POST") { //when hits 'submit'- checks and corrects if neccesery the input
   $sitename = test_input($_POST["sitename"]);
   $password = test_input($_POST["password"]);
   $highlight_deviation = test_input($_POST["highlight_deviation"]);
   $site_update_interval = test_input($_POST["site_update_interval"]);
   $station_update_interval = test_input($_POST["station_update_interval"]);
   $time = test_input($_POST["time"]);
   $time_update_interval = test_input($_POST["time_update_interval"]);
   $tech1 = test_input($_POST["tech1"]);
   $tech2 = test_input($_POST["tech2"]);
   $wind = test_input($_POST["wind"]);
   $changed=1;
}

?>

	<title>Solar panels - setup page</title>
	<p align='center'> <font color=#1B325F size='10pt'>Setup</font></p>
<center><form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
   <br><br>
   Site password: <input type="password" name="password" value="<?php echo $password;?>">
   <br><br>
   Site Name: <input type="text" name="sitename" value="<?php echo $sitename;?>" required>
   <br><br>
   Highlight deviation: <input type="number" name="highlight_deviation" value="<?php echo $highlight_deviation;?>" min="1" max="5" step="0.2">
   <br><br>
   Site Update Interval: <input type="number" name="site_update_interval" value="<?php echo $site_update_interval;?>" min="1" max="5" step="0.2">
   <br><br>
   Station Update Interval: <input type="number" name="station_update_interval" value="<?php echo $station_update_interval;?>"  min="1" max="30" step="1">
   <br><br>
   Time: <input type="text" name="time" value="<?php echo $time;?>">
   <br><br>
   Time Update Interval: <input type="number" name="time_update_interval" min="1" max="200" value="<?php echo $time_update_interval;?>">
   <br><br>
   Tech parameter: <input type="number" name="tech1" min="1" max="30" value="<?php echo $tech1;?>">
		   <input type="number" name="tech2" min="1" max="30" value="<?php echo $tech2;?>">
   <br><br>
   Read wind read from station: <input type="number" name="wind" min="1" max="50" value="<?php echo $wind;?>">
   <br><br>
   <input type="submit" name="submit" value="Submit"> 
</form></center>

<?php
function fpcheck($fp) {
//	if (!file_exists($fp)) {
//	  echo file_exists($fp);}
//	else if(!$fh = fopen($fp, 'w')) {
//	  echo 'Can\'t open file';}
//	else {
//	  echo 'Success open file';}*/
}
if ($changed==1){ //to only change and show changes after a requested change, and not on startup/ refresh
	echo "<h2>Your Input:</h2>";
		echo "password: ",$password;
		$fp = fopen("txt/pass.txt", "w");	
		fpcheck($fp);
		fwrite($fp,$password);
		fclose($fp);
	echo "   |   ";
		echo "sitename: ",$sitename;
		$fp = fopen("txt/sitename.txt", "w");	
		fpcheck($fp);
		fwrite($fp,$sitename);
		fclose($fp);
	echo "   |   ";
		echo "highlight_deviation: ",$highlight_deviation;
		$fp = fopen("txt/highlight_deviation.txt", "w");	
		fpcheck($fp);
		fwrite($fp,$highlight_deviation);
		fclose($fp);
	echo "   |   ";
		echo "site_update_interval: ",$site_update_interval;
		$fp = fopen("txt/site_update_interval.txt", "w");	
		fpcheck($fp);
		fwrite($fp,$site_update_interval);
		fclose($fp);
	echo "<br>";
		echo "station_update_interval: ",$station_update_interval;
		$fp = fopen("txt/station_update_interval.txt", "w");	
		fpcheck($fp);
		fwrite($fp,$station_update_interval);
		fclose($fp);
	echo "   |   ";
		echo "time: ",$time;
		$fp = fopen("txt/time.txt", "w");	
		fpcheck($fp);
		fwrite($fp,$time);
		fclose($fp);
	echo "   |   ";
		echo "time_update_interval: ",$time_update_interval;
		$fp = fopen("txt/time_update_interval.txt", "w");	
		fpcheck($fp);
		fwrite($fp,$time_update_interval);
		fclose($fp);
	echo "   |   ";
		echo "tech1: ",$tech1;
		$fp = fopen("txt/tech1.txt", "w");	
		fpcheck($fp);
		fwrite($fp,$tech1);
		fclose($fp);
		echo "tech2: ",$tech2;
		$fp = fopen("txt/tech2.txt", "w");	
		fpcheck($fp);
		fwrite($fp,$tech2);
		fclose($fp);
         echo "   |   ";
                 echo "Read wind read from station: ",$wind;
                 $fp = fopen("txt/wind.txt", "w");
                 fpcheck($fp);
                 fwrite($fp,$wind);
                 fclose($fp);

}
?>

</body>
</html>
