<html>
<head>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">

var data_array = [<?php
$fsum = fopen("graph.csv", "r");
$first_flag=false;
while (($line = fgetcsv($fsum)) !== false) {
	if ($first_flag) 
		echo ",[" ;
	else
	{
		$first_flag=true;
		echo "[" ;
	}
	for ($i=0;$i<count($line)-1;$i++)
		echo  $line[$i]  . ",";
	echo $line[count($line)-1];
	echo "]";
	$sz = count($line);
}
fclose($fsum);
?>
];
data_array.unshift(['Time' <?php
for ($x=1;$x<$sz/2;$x++)
{
	for ($y=0;$y<2;$y++)
		echo ",'Station".$x.":".($y+1)."'";
}
?>]);
//document.write(data_array.toString() );

google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawChart);
function drawChart() {
	var data = google.visualization.arrayToDataTable(data_array);
	var options = {
title: 'Angles chart'
	};
	var chart = new google.visualization.LineChart(document.getElementById('chart_div'));

	chart.draw(data, options);


	var hideSal = document.getElementById("hideSales");
	hideSal.onclick = function()
	{
		view = new google.visualization.DataView(data);
		view.hideColumns([1]); 
		chart.draw(view, options);
	}
	var hideExp = document.getElementById("hideExpenses");
	hideExp.onclick = function()
	{
		view = new google.visualization.DataView(data);
		view.hideColumns([2]); 
		chart.draw(view, options);
	}
	var hideSal = document.getElementById("showSales");
	hideSal.onclick = function()
	{
		view = new google.visualization.DataView(data);
		view.showColumns([1]); 
		chart.draw(view, options);
	}
	var hideExp = document.getElementById("showExpenses");
	hideExp.onclick = function()
	{
		view = new google.visualization.DataView(data);
		view.showColumns([2]); 
		chart.draw(view, options);
	}



}


</script>

<body>
<div id="chart_div" style="width: 900px; height: 500px;"></div>

<button type="button" id="hideSales"  >Hide Sales</button>
<button type="button" id="showSales"  >Show Sales</button>
<button type="button" id="hideExpenses"  >Hide Expence</button>
<button type="button" id="showExpenses"  >Show Expence</button>

</body>
</html>
