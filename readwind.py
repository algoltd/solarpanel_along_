import sys
import serial
from time import time
from operator import xor

################# DOCUMANTIONS ################

#        ___Read angle___
#Request:
# Header | Station NO | opcode | Checksum
# 0xA5   |            | 0x11   | computed
#Reply:
# Header | Station NO | angle1 | angle2 | Checksum
# 0x53   |            |        |        |

#        ___Read Wind Speed___
#Request:
# Header | Station NO | opcode | Checksum
# 0xA5   |            | 0x22   | computed
#Reply:
# Header | Station NO | speed  | Checksum
# 0x53   |            |        |

#        ___Set Angle___
#Request:
# Header | Station NO | opcode | Angle Select | Angle Valus | Checksum
# 0xA5   |            | 0x33   |              |             |
#Reply:
# Header | Station NO | ACK  / NACK | Checksum
# 0x53   |            | 0xA0 / 0xAF |

#        ___Set Time___
#Request:
# Header | Station NO | opcode | Hour (24 format) | Min | Checksum
# 0xA5   |            | 0x66   |                  |     |
#Reply:
# Header | Station NO | ACK  / NACK | Checksum
# 0x53   |            | 0xA0 / 0xAF |

################################################

################### FUNCTIONS ##################

###aquisiton_status_func reports of thhe status of the aquisition
def aquisition_status_func(aquire_bool):

   if aquire_bool==1:
      if ( cmp_chksm_func(rcv_stationNo, rcv_data) == int(rcv_chksm,16) ):
#         print "The chksm is OK, chksm=", rcv_chksm ,file=sys.stderr
         if rcv_data.isdigit():
            print rcv_data
            break
   print "***"  #for all other cases


###cmp_chksm_func is a function that gets 2 str as inputs, 
###and returns (as int) the checksum of both of them, bit by bit.
def cmp_chksm_func(str1, str2):
   cmp_chksm= int(str1) ^ int(str2)
   return cmp_chksm


###out_checksum_func is a function that gets 2 inputs in a hex form, e.g: "\xFF"
###computes their checksum (bit by bit) and returns the answer also in the same hex form
def out_checksum_func(out_stationNo, out_opcode):
   out_checksum1=cmp_chksm_func( str(out_stationNo.encode('hex')),str(out_opcode.encode('hex')) )
   out_checksum_strHex = "0x%d" % (out_checksum1)
   return chr(int(out_checksum_strHex,16))


###select_opcode_func is a function that recieves a command to write to the board,
###and translate it to the corrospoding hex opcode
def select_opcode_func(command):
   if command=="read_angle":
      out_opcode="\x11"
   if command=="read_wind_speed":
      out_opcode="\x22"
   if command=="set_angle":
      out_opcode="\x33"
   if command=="set_time":
      out_opcode="\x66"
   return out_opcode
################################################


port = "/dev/ttyAMA0"
ser = serial.Serial(port,9600)
ser.flushInput()

#print "serial connection details:\n", ser   ,file=sys.stderr #prints the serial connection details


req_command="read_wind_speed" #1 of the 4 command options
#req_stationNo=1
#operate_func(req_command, req_stationNo)


out_header="\xA5" #the usual Request header
#req_stationNo=input("Enter station No. ")
out_stationNo=sys.argv[1] #chr(req_stationNo)
out_opcode=select_opcode_func(req_command)
out_checksum=out_checksum_func(out_stationNo,out_opcode)

out_message=out_header + out_stationNo + out_opcode + out_checksum
ser.write(out_message)


rcv_counter=0
answer=[]
timer_start=time() #epoch unix time stamp

# pooling loop, takes 1byte 4times, breaks if not finished in 250 [ms]
while 1:
   #print "inside loop"
   if( ser.inWaiting()>0 ) :   #number of bytes in the input buffer
      #print("in the loop", counter)
      receive = ser.read(1)
      #print 'Hex value: ', receive.encode('hex')
      rcv_counter=rcv_counter+1
      if rcv_counter==1:
         rcv_header= receive.encode('hex')
         answer.append(rcv_header)
         #print 'rcv_header, Hex value: ', rcv_header
      if rcv_counter==2:
         rcv_stationNo=receive.encode('hex')
         answer.append(rcv_stationNo)
         #print 'rcv_stationNo, Hex value: ', rcv_stationNo
      if rcv_counter==3:
         rcv_data=receive.encode('hex')
         answer.append(rcv_data)
         #print 'rcv_data, Hex value: ', rcv_data
      if rcv_counter==4:
         if req_command=="read_angle":
            rcv_data=receive.encode('hex')
            answer.append(rcv_data)
         else:
            rcv_chksm=receive.encode('hex')
            answer.append(rcv_chksm)
            #print 'rcv_chksm, Hex value: ', rcv_chksm
            aquire_bool=1
            break #because there will be no more inputs from the device later
      if rcv_counter==5:
         rcv_chksm=receive.encode('hex')
         answer.append(rcv_chksm)
         #print 'rcv_chksm, Hex value: ', rcv_chksm
         aquire_bool=1
         break #because there will be no more inputs from the device later

   timer_end=time()
   timer_delta= timer_end-timer_start
   #print timer_delta
   if timer_delta>0.250: #if the timer delta is more than 250 [mili sec]
      aquire_bool=0
      break

#print "Answer:", answer ,file=sys.stderr
aquisition_status_func(aquire_bool)

